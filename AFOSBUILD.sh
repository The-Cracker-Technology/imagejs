mkdir build

cd build

cmake ../

make

strip imagejs

cp -Rf imagejs /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
